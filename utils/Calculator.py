from math import sqrt

class Calculator:
  @staticmethod
  def calculateCentroid(points):
    centroid = [0, 0] 
    signedArea = 0
    n = len(points) 
  
    # For all vertices 
    for i in range(len(points)): 
      x0 = points[i]["x"] 
      y0 = points[i]["y"] 
      x1 = points[(i + 1) % n]["x"] 
      y1 = points[(i + 1) % n]["y"] 

      # Calculate value of A 
      # using shoelace formula 
      A = (x0 * y1) - (x1 * y0) 
      signedArea += A 

      # Calculating coordinates of 
      # centroid of polygon 
      centroid[0] += (x0 + x1) * A 
      centroid[1] += (y0 + y1) * A 
  
    signedArea *= 0.5
    dividendFactor = 6 * signedArea
    centroid[0] = round((centroid[0]) / dividendFactor, 2)
    centroid[1] = round((centroid[1]) / dividendFactor, 2)

    return { "x": centroid[0], "y": centroid[1] } 
  
  @staticmethod
  def calculateEuclideanDistance(robot_position, centroid):
    return sqrt(pow((centroid["x"] - robot_position["x"]), 2) +
                pow((centroid["y"] - robot_position["y"]), 2))