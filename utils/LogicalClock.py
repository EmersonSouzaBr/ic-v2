from abstract import SingletonMeta

class LogicalClock():
  __metaclass__ = SingletonMeta
  def __init__(self):
    self._timestamp = 0

  def tick(self, otherTimestamp = None):
    if otherTimestamp is int: 
      self._timestamp += 1
      return
    else:
      self._timestamp = self._compareTimestamps(otherTimestamp) + 1

  def getTimestamp(self):
    return self._timestamp
  
  def _compareTimestamps(self, otherTimestamp):
    return max(self._timestamp, otherTimestamp)