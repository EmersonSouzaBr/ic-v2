from enum import Enum
from models.NodeInfo import NodeInfo

class Channel(Enum):
  multicast = 1
  unicast = 2

class MessageType(Enum):
  command = 1
  control = 2

class Message:
  def __init__ (self, channel, message_type, command, node_info, logical_clock_value = None):
    if (not isinstance(node_info, NodeInfo)):
      raise Exception("'node_info' must be a NodeInfo instance.")
    if (channel == Channel.multicast and logical_clock_value == None):
      raise Exception("A message needs a Logical Clock value")

    self.header = {
      "channel": channel,
      "message_type": message_type,
      "metadata": {
        "origin": node_info,
        "timestamp": logical_clock_value
      }
    }
    self.content = command