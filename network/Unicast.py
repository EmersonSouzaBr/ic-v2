import sys
sys.path.append("..")

import socket, pickle
from threading import Thread
from commands import MoveCommand, StartCommand
from Message import Message, Channel, MessageType
from CommandFactory import CommandFactory
from memory import RobotState

class Unicast():
  def __init__(self, host, port, node_info):
    self.node_info = node_info
    self._host = host
    self._port = port
    Thread(target=self.listen).start()

  def listen(self):
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    orig = (self._host, self._port)
    udp.bind(orig)
    while True:
      msg, client = udp.recvfrom(1024)
      Thread(target=self.processMessage, args=(pickle.loads(msg),)).start()
    udp.close()

  def sendControl (self, toHost, toPort, control):
    control = Message(
      Channel.unicast,
      MessageType.control,
      control,
      self.node_info,
      None
    )
    self.send(toHost, toPort, control)
    pass

  def send(self, host, port, message):
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    dest = (host, port)
    udp.sendto(pickle.dumps(message), dest)
    pass

  def processMessage(self, message):
    if (not isinstance(message, Message)): return
    metadata = message.header["metadata"]
    message_type = message.header["message_type"]
    state = RobotState()
    
    # If message is from itself
    if (metadata["origin"].node_id == state.node_info.node_id): return
    
    if (message_type == MessageType.command):
      command = message.content
      command_name = command["command_name"]
      command_params = command["command_params"]
    elif (message_type == MessageType.control):
      control = message.content
      control_name = control["command_name"]
      control_params = control["command_params"]
      if (control_name == "ping"):
        host = metadata["origin"].ip
        port = metadata["origin"].port
        self.sendControl(host, port, CommandFactory.pong(state.node_info))
      elif (control_name == "pong"):
        if (state.isCoordinator(metadata["origin"])):
          state.coordinatorChecked()