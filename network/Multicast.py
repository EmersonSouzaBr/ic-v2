import socket, multiprocessing, struct, pickle
from threading import Thread
from memory import RobotState
from Protocol import Protocol
from CommandFactory import CommandFactory
from Message import Message, Channel, MessageType
from utils import LogicalClock
from commands import MoveCommand, PositionCommand

class Multicast:
  def __init__(self, node_info):
    self.group_ip = '224.1.1.1'
    self.port = 6000
    self.worker = None
    self.node_info = node_info
    self.logical_clock = LogicalClock()
    self.nodes_alive = []

  def start(self):
    if self.worker is not None:
      self.worker.terminate()
    self.worker = Thread(target=self.listen)
    self.worker.start()
  
  def listen(self):
    address = (self.group_ip, self.port)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    group = socket.inet_aton(address[0])
    mreq = struct.pack('4sL', group, socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(address)

    while True:
      data, address = sock.recvfrom(1024)
      Thread(target=self.processMessage, args=(pickle.loads(data),)).start()
    pass

  def sendCommand (self, command):
    self.logical_clock.tick()
    command = Message(
      Channel.multicast,
      MessageType.command,
      command,
      self.node_info,
      self.logical_clock.getTimestamp()
    )

    self.send(command)
    pass
  
  def sendControl (self, control):
    self.logical_clock.tick()
    control = Message(
      Channel.multicast,
      MessageType.control,
      control,
      self.node_info,
      self.logical_clock.getTimestamp()
    )
    self.send(control)
    pass

  def send (self, message):
    if (not isinstance(message, Message)):
      raise Exception("'message' must be an instance of Message")

    address = (self.group_ip, self.port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(0.2)
    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
    
    sent = sock.sendto(pickle.dumps(message), address)

  def processMessage(self, message):
    if (not isinstance(message, Message)): return
    
    metadata = message.header["metadata"]
    message_type = message.header["message_type"]
    state = RobotState()
    
    # If message is from itself
    if (metadata["origin"].node_id == state.node_info.node_id): return
    
    if(message.header["channel"] == Channel.multicast):
      self.clock = LogicalClock()
      self.clock.tick(metadata["timestamp"])
    print(metadata["timestamp"], self.clock.getTimestamp())
    if (message_type == MessageType.command):
      command = message.content

      command_name = command["command_name"]
      command_params = command["command_params"]
      
      if (command_name == "move" and state.isCoordinator(metadata["origin"])):
        command = MoveCommand(
          state.node_info.node_id,
          command_params["linear_coords"],
          command_params["angular_coords"]
        )
        command.execute()
      pass
    elif (message_type == MessageType.control):
      control = message.content

      control_name = control["command_name"]
      control_params = control["command_params"]

      if (control_name == "search_coordinator"):
        if (state.iAmCoordinator()):
          self.sendControl(CommandFactory.coordinate(state.node_info))
          pass
      elif (control_name == "update_coordinator"):
        print("Atualizando Coordenador")
        state.bindCoordinator(control_params["coordinator_info"])
        state.coordinatorChecked()
        pass
      elif (control_name == "coordinator_down"):
        print("Coordinator Down")
        command = PositionCommand(state.node_info.node_id)
        position = command.execute()
        state.unbindCoordinator()
        state.bindElectionManager(control_params["manager_info"])
        state.setElectionStatus(True, metadata["timestamp"])
        if (state.isElectionManager(metadata["origin"])):
          self.sendControl(CommandFactory.iAmAlive(state.node_info, position, state.battery))
          state.clearNodes()
        pass
      elif (control_name == "i_am_alive"):
        if (state.iAmElectionManager()):
          state.appendNodeAlive(
            control_params["node_info"],
            control_params["node_position"],
            control_params["node_battery"]
          )
        pass
      elif (control_name == "start_election"):
        print("Election started")
        pass
      elif (control_name == "election_fail"):
        state.unbindCoordinator()
        state.bindElectionManager(control_params["manager_info"])
        pass
      pass