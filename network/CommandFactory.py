# Esta classe abstrai a criacao dos commandos que devem ser enviados por rede.

from models.NodeInfo import NodeInfo

class CommandFactory:
  @staticmethod
  def move(linear_coords, angular_coords):
    if (type(linear_coords) is not list):
      raise Exception("'linear_coords' must be a list.")
    if (type(angular_coords) is not list):
      raise Exception("'angular_coords' must be a list.")

    return {
      "command_name": "move",
      "command_params": {
        "linear_coords": linear_coords,
        "angular_coords": angular_coords
      }
    }
  
  @staticmethod
  def join(node_info):
    return {
      "command_name": "join",
      "command_params": {
        "node_info": node_info
      },
    }

  @staticmethod
  def coordinate(node_info):
    if (not isinstance(node_info, NodeInfo)):
      raise Exception("'node_info' must be a NodeInfo instance.")
    return {
      "command_name": "update_coordinator",
      "command_params": {
        "coordinator_info": node_info
      },
    }

  @staticmethod
  def searchCoordinator():
    return {
      "command_name": "search_coordinator",
      "command_params": None
    }

  @staticmethod
  def coordinatorDown(node_info):
    return {
      "command_name": "coordinator_down",
      "command_params": {
        "manager_info": node_info
      }
    }

  @staticmethod
  def iAmAlive(node_info, position, battery):
    return {
      "command_name": "i_am_alive",
      "command_params": {
        "node_info": node_info,
        "node_position": position,
        "node_battery": battery
      }
    }

  @staticmethod
  def ping(node_info):
    if (not isinstance(node_info, NodeInfo)):
      raise Exception("'node_info' must be a NodeInfo instance.")
    return {
      "command_name": "ping",
      "command_params": {
        "node_info": node_info,
      }
    }

  @staticmethod
  def pong(node_info):
    if (not isinstance(node_info, NodeInfo)):
      raise Exception("'node_info' must be a NodeInfo instance.")
    return {
      "command_name": "pong",
      "command_params": {
        "node_info": node_info,
      }
    }