from abstract import SingletonMeta
from models.NodeInfo import NodeInfo
import random

class RobotState():
  __metaclass__ = SingletonMeta

  def __init__(self):
    self.node_info = None
    self.coordinator = None
    self.position = [0, 0]
    self.election_manager = None
    self.battery = random.randint(20, 100)
    self.nodes_alive = []
    self.coordinator_status = {
      "election_start_timestamp": False,
      "election_started": False,
      "checking": False,
      "response_success": False
    }
  
  def bindCoordinator(self, coordinator_info):
    if (isinstance(coordinator_info, NodeInfo)):
      self.coordinator = coordinator_info
  
  def unbindCoordinator(self):
    self.coordinator = None

  def bindElectionManager(self, manager_info):
    if (isinstance(manager_info, NodeInfo)):
      self.election_manager = manager_info
  
  def unbindCoordinator(self):
    self.election_manager = None
  
  def bindNodeInfo(self, node_info):
    if (isinstance(node_info, NodeInfo)):
      self.node_info = node_info
  
  def iAmCoordinator(self):
    if (self.coordinator == None):
      return False
    return self.coordinator.node_id == self.node_info.node_id
  
  def iAmElectionManager(self):
    if (self.election_manager == None):
      return False
    return self.election_manager.node_id == self.node_info.node_id

  def isCoordinator(self, node_info):
    if (isinstance(node_info, NodeInfo)):
      return self.coordinator.node_id == node_info.node_id
    else:
      return False
  
  def isElectionManager(self, node_info):
    if (isinstance(node_info, NodeInfo)):
      return self.election_manager.node_id == node_info.node_id
    else:
      return False

  def bindPosition(self, x, y):
    self.position[0] = x
    self.position[1] = y
    return self.position

  def appendNodeAlive(self, node_info, node_position, node_battery):
    if (isinstance(node_info, NodeInfo)):
      self.nodes_alive.append({
        "node_info": node_info,
        "node_position": node_position,
        "node_battery": node_battery
      })
  
  def clearNodes (self):
    self.nodes_alive = []

  def setElectionStatus(self, election_started, timestamp = None):
    if (type(election_started) is bool):
      self.coordinator_status["election_started"] = election_started
      if (election_started == True):
        self.coordinator_status["election_started_timestamp"] = timestamp
      else:
        self.coordinator_status["election_started_timestamp"] = None
  
  def startCheckCoordinator(self):
    if (self.coordinator_status["election_started"] == False):
      self.coordinator_status["checking"] = True
      self.coordinator_status["response_success"] = False
  
  def coordinatorChecked(self):
    self.coordinator_status["checking"] = False
    self.coordinator_status["response_success"] = True
    self.coordinator_status["election_started"] = False
    self.coordinator_status["election_started_timestamp"] = None