from memory import RobotState
from network.Protocol import Protocol
from network.Message import Channel, MessageType, Message
from network.Multicast import Multicast
from network.Unicast import Unicast
from commands import StartCommand, MoveCommand, PositionCommand
from models.NodeInfo import NodeInfo
from network.CommandFactory import CommandFactory
from threading import Timer, Thread
from utils.Calculator import Calculator
import rospy, time, random, sys

class Robot:
  def __init__(self, name, x, y, port):
    self.node_id = name
    self.unicast_ip = "localhost"
    self.unicast_port = port
    self.node_info = NodeInfo(self.node_id, self.unicast_ip, self.unicast_port)
    self.multicast = Multicast(self.node_info)
    self.unicast = Unicast(self.unicast_ip, self.unicast_port, self.node_info)
    self._coordinator_retries = 3
    state = RobotState()
    state.bindNodeInfo(self.node_info)
    state.bindPosition(x, y)
    rospy.init_node(self.node_id)
    self.multicast.start()

  def startNode(self, x, y, theta):
    command = StartCommand(self.node_id)
    command.execute(x, y, theta)
  
  def joinToGroup(self):
    control = CommandFactory.join(self.node_info)
    self.multicast.sendControl(control)
  
  def promoteCoordinator (self):
    control = CommandFactory.coordinate(self.node_info)
    self.multicast.sendControl(control)

  def move(self):
    state = RobotState()
    if (state.iAmCoordinator()):
      linear_coords = [random.randint(3, 7), 0.0, random.randint(3, 6)]
      angular_coords = [0.0, 0.0, 1.1]
      move = MoveCommand(self.node_id, linear_coords, angular_coords)
      move.execute()
      command = CommandFactory.move(linear_coords, angular_coords)
      
      self.multicast.sendCommand(command)
    
  def getPosition(self):
    c = PositionCommand(self.node_id)
    c.execute()

  def searchCoordinator(self):
    state = RobotState()
    
    if state.coordinator is None:
      if self._coordinator_retries > 0:
        print "Searching coordinator. Try {0}".format(self._coordinator_retries)
        control = CommandFactory.searchCoordinator()
        self.multicast.sendControl(control)
        self._coordinator_retries -= 1
        Timer(random.randint(2, 3), self.searchCoordinator).start()
      elif self._coordinator_retries == 0:
        state.bindCoordinator(self.node_info)
        print "Coordenador definido"

  def checkCoordinatorStatus(self):
    state = RobotState()
    if (state.coordinator is not None and not state.iAmCoordinator()):
      coordinator = state.coordinator
      state.startCheckCoordinator()
      self.unicast.sendControl(coordinator.ip, coordinator.port, CommandFactory.ping(self.node_info))
      time.sleep(2)
      if (not state.coordinator_status["response_success"] and not state.coordinator_status["election_started"]):
        self.multicast.sendControl(CommandFactory.coordinatorDown(self.node_info))
        state.bindElectionManager(self.node_info)
        print ("Aguardando Nodes")
        time.sleep(3)
        self.processElection()
      Timer(random.randint(2, 4), self.checkCoordinatorStatus())

  def processElection(self):
    state = RobotState()
    command = PositionCommand(self.node_id)
    position = command.execute()
    state.appendNodeAlive(self.node_info, position, state.battery)
    
    qty_nodes =  len(state.nodes_alive)
    new_coordinator = None

    if qty_nodes == 1:
      new_coordinator = self.node_info
    elif qty_nodes == 2:
      nodes = state.nodes_alive
      if (nodes[0]["node_battery"] <= nodes[1]["node_battery"]):
        new_coordinator = nodes[0]["node_info"]
      else:
        new_coordinator = nodes[1]["node_info"]
    elif qty_nodes >= 2:
      POSITION_INDEX = 1
      points = [node["node_position"] for node in state.nodes_alive]
      print points
      centroid = Calculator.calculateCentroid(points)
      new_coordinator = self._getClosestToCentroid(state.nodes_alive, centroid)
    
    state.clearNodes()
    self._publishNewCoordinator(new_coordinator)

  def _getClosestToCentroid(self, nodes, centroid):
    closest = None
    shortest_distance = float('inf')
    shortest_battery = float('inf')

    for node in nodes:
      distance = Calculator.calculateEuclideanDistance(node["node_position"], centroid)
      if (distance < shortest_distance):
        shortest_distance = distance
        shortest_battery = node["node_battery"]
        closest = node["node_info"]
      elif (distance == shortest_distance and node["node_battery"] < shortest_battery):
        shortest_distance = distance
        shortest_battery = node["node_battery"]
        closest = node["node_info"]
    
    return closest

  def _publishNewCoordinator(self, new_coordinator_info):
    state = RobotState()
    if (isinstance(new_coordinator_info, NodeInfo)):
      state.bindCoordinator(new_coordinator_info)
      state.coordinatorChecked()
      self.multicast.sendControl(CommandFactory.coordinate(new_coordinator_info))

if __name__ == "__main__":
  try:
    name = raw_input("Turtle Name: ")
    x = raw_input("Position - X: ")
    y = raw_input("Position - Y: ")
    port = int(raw_input("Unicast Port:"))
    robot = Robot(name, x, y, port)
    robot.startNode(x, y, 3)
    robot.joinToGroup()
    robot.searchCoordinator()
    robot.getPosition()
    robot.checkCoordinatorStatus()
    while True:
      robot.move()
      time.sleep(2)
  except (KeyboardInterrupt, SystemExit):
    print("opa")
    sys.exit()
