from BaseCommand import BaseCommand
from turtlesim.msg._Pose import Pose
from memory import RobotState
import rospy

class PositionCommand(BaseCommand):
  def __init__(self, node_name):
    self.topic_name = node_name + '/pose'
    self.x = None
    self.y = None

  def execute(self):
    try:
      state = RobotState()
      pose = rospy.wait_for_message(self.topic_name, Pose)
      self.x = pose.x
      self.y = pose.y
      state.bindPosition(self.x, self.y)
      return { "x": self.x, "y": self.y }
    except Exception as e:
      print(e)
      pass
