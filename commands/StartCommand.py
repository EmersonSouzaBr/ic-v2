from commands.BaseCommand import BaseCommand
from turtlesim.srv._Spawn import Spawn
import os
import rospy

class StartCommand(BaseCommand):
  def __init__(self, node_name):
    self.node_name = node_name
  
  def execute(self, x, y, z):
    try:
      serv = rospy.ServiceProxy('spawn', Spawn)
      serv(float(x), float(y), float(z), self.node_name)
    except Exception as e:
      print('Node already exists')
      pass
