from abc import abstractmethod, ABCMeta

class BaseCommand():
  __metaclass__ = ABCMeta
  
  @abstractmethod
  def execute(self):
    pass