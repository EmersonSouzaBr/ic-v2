from BaseCommand import BaseCommand
from geometry_msgs.msg import Twist
import rospy

# rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 1.1]'
class MoveCommand(BaseCommand):
  def __init__(self, node_name, linear_coord, angular_coord):
    self.topic_name = node_name + '/cmd_vel'
    self._linear_coord = linear_coord
    self._angular_coord = angular_coord

  def execute(self):
    try:
      pub = rospy.Publisher(self.topic_name, Twist, queue_size = 10)
      vel_msg = Twist()
      vel_msg.linear.x = self._linear_coord[0]
      vel_msg.linear.y = self._linear_coord[1]
      vel_msg.linear.z = self._linear_coord[2]
      vel_msg.angular.x = self._angular_coord[0]
      vel_msg.angular.y = self._angular_coord[1]
      vel_msg.angular.z = self._angular_coord[2]
      pub.publish(vel_msg)
      rospy.Rate(0.5).sleep()
    except Exception as e:
      print(e)
      pass
